#!/bin/bash

# Remove server.pid file from the host system
rm -f ./tmp/pids/server.pid

# delete all containers on system
if [ "$(docker ps -aq)" ]; then docker rm -f $(docker ps -aq); fi

# delete all images on system
if [ "$(docker images -q)" ]; then docker rmi -f $(docker images -q); fi

# Remove all Docker volumes
# if [ "$(docker volume ls -q)" ]; then
#     docker volume rm -f $(docker volume ls -q)
# fi

docker-compose up -d