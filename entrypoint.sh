#!/bin/bash
# set -e


wait_for_db() {
  echo "Waiting for MySQL..."
  while true; do
    # Attempting to ping MySQL at host "db" on port 3306
    mysqladmin ping -h "db" -P "3306" -u "root" --password="123" &>/dev/null
    if [[ $? -eq 0 ]]; then
      echo "MySQL is ready."
      break
    else
      echo "MySQL is unavailable - sleeping."
      sleep 8
    fi
  done
}

# Call the wait_for_db function
wait_for_db

# Uncomment the line below if you need to create the database
# rake db:create

# Run database migrations
rake db:migrate


# Then execute the main process (CMD)
exec "$@"