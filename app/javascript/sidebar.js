// app/javascript/sidebar.js

document.addEventListener('DOMContentLoaded', () => {
  const sidebarToggle = document.querySelector('#sidebarToggle');

  if (sidebarToggle) {
    sidebarToggle.addEventListener('click', (event) => {
      toggleSidebar(event);
    });
  }
});

function toggleSidebar(event) {
  event.preventDefault();

  const customBody = document.getElementById('custombody');

  if (customBody) {
    customBody.classList.toggle('sb-sidenav-toggled');
    localStorage.setItem('sb|sidebar-toggle', customBody.classList.contains('sb-sidenav-toggled').toString());
  } else {
    console.error('Custom body element not found!');
  }
}
