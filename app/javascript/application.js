// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "popper"
import "bootstrap"

document.addEventListener('DOMContentLoaded', () => {
    const logoutButton = document.getElementById('logout-btn');
  
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        fetch('/users/sign_out', {
          method: 'DELETE',
          headers: {
            'X-CSRF-Token': document.querySelector("[name='csrf-token']").content
          }
        })
        .then(response => {
          if (response.redirected) {
            window.location.href = response.url;
          }
        });
      });
    }
  });
  