class CategoriesController < ApplicationController
  before_action :authenticate_user!

  def index
    @categories = current_user.categories
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    @category.user = current_user  # Assuming you have a current_user method from Devise or similar

    begin
      if @category.save
        # redirect_to some_path, notice: 'Category was successfully created.'
        redirect_to categories_path, notice: "Category was successfully created."
      else
        render :new
      end
    rescue ActiveRecord::RecordNotUnique
      @category.errors.add(:name, "has already been taken")
      render :new
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update(category_params)
      redirect_to categories_path, notice: "Category was successfully updated."
    else
      render :edit
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
